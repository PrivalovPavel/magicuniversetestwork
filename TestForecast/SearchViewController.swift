//
//  SearchViewController.swift
//  TestForecast
//
//  Created by Павел Привалов on 6/14/18.
//  Copyright © 2018 Павел Привалов. All rights reserved.
//

import UIKit
import CoreData
import SwiftyJSON

class SearchViewController: UIViewController {
	
	@IBOutlet weak var searchBar: UISearchBar!
	@IBOutlet weak var activityIndicator: UIActivityIndicatorView!
	@IBOutlet weak var tableView: UITableView!
	
	let dispatchGroup = DispatchGroup()
	let forecast = ForecastService()
	var forecastJSON = JSON()
	var cities = [City]()
	var filteredArray = [City]()
	let arrayIndexSection = ["A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z", ""]
	var selectedIndex = 0
	let segueButtonString = "add"
	

	override func viewDidLoad() {
		super.viewDidLoad()
		
		UIViewController.setFinishLoading(isFinished: false, activityIndicator: self.activityIndicator, hiddenView: self.tableView)

		searchBar.setupSearchBarTextfield(UIColor.lightBackground, UIColor.white)
		searchBar.delegate = self
		
		loadCoreData()
    }
	
	func loadForecast(lon: Double, lat: Double) {
		dispatchGroup.enter()
		forecast.getForecast(lon: lon, lat: lat) { (completion) in
			if completion != nil {
				print(completion!)
				self.forecastJSON = completion!
				self.dispatchGroup.leave()
			} else {
				let alert = UIAlertController(title: "Error", message: "We can't get forecast", preferredStyle: UIAlertControllerStyle.alert)
				alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
			}
		}
	}

	func loadCoreData() {
		let fetchRequest: NSFetchRequest<City> = City.fetchRequest()
		let queue = DispatchQueue.global(qos: .utility)
		let workItem = DispatchWorkItem {
			do
			{
				let cities = try DatabaseController.getContext().fetch(fetchRequest)
				self.cities = cities
			}
			catch
			{
				print("error \(error)")
			}
		}
		queue.async(execute: workItem)
		workItem.notify(queue: DispatchQueue.main) {
			self.filteredArray = self.cities
			self.tableView.reloadData()
			UIViewController.setFinishLoading(isFinished: true, activityIndicator: self.activityIndicator, hiddenView: self.tableView)
		}
	}
	
	func loadJsonData(filename: String) {
		if let path = Bundle.main.path(forResource: filename, ofType: "json")
		{
			do
			{
				let data = try Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe)
				let jsonResult = try JSONSerialization.jsonObject(with: data, options: .mutableLeaves)
				if let jsonResult = jsonResult as? [Dictionary<String, AnyObject>]
				{
					for result in jsonResult
					{
						let city: City = City(context: DatabaseController.getContext())
						
						city.id = result["id"] as! Int32
						city.name = result["name"] as? String
						city.country = result["country"] as? String
						city.lon = result["coord"]!["lon"] as! Double
						city.lat = result["coord"]!["lat"] as! Double
						
						cities.append(city)
					}
					DatabaseController.saveContext()
				}
			} catch {
				print("parse error: \(error)")
			}
		}
	}

	override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
		self.view.endEditing(true)
	}
}

extension SearchViewController: UISearchBarDelegate
{
	func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
		guard !searchText.isEmpty else {
			filteredArray = cities
			tableView.reloadData()
			return
		}
		
		let searchTextLovercased = searchText.lowercased()
		
		filteredArray = cities.filter({ (city) -> Bool in
			(city.name?.lowercased().contains(searchTextLovercased))!
		})
		tableView.reloadData()
	}
}

extension SearchViewController: UITableViewDelegate, UITableViewDataSource
{
	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return filteredArray.count
	}
	
	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		let cell = tableView.dequeueReusableCell(withIdentifier: "tableCell", for: indexPath) as! CustomTableViewCell
		cell.cityNameLabel.text = filteredArray[indexPath.row].name
		return cell
	}
	
	func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
		selectedIndex = indexPath.row
		loadForecast(lon: filteredArray[indexPath.row].lon, lat: filteredArray[indexPath.row].lat)
		dispatchGroup.notify(queue: DispatchQueue.main) {
			self.performSegue(withIdentifier: "showNewCity", sender: nil)
		}
	}
	
	override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
		if segue.identifier == "showNewCity"
		{
			let newView = segue.destination as! CityForecastViewController
			
			newView.forecastJSON = forecastJSON
			newView.city = filteredArray[selectedIndex]
		}
	}
}

extension UIViewController {
	class func setFinishLoading(isFinished: Bool, activityIndicator: UIActivityIndicatorView, hiddenView: UIView) {
		activityIndicator.isHidden = isFinished
		isFinished ? activityIndicator.stopAnimating() : activityIndicator.startAnimating()
		hiddenView.isHidden = !isFinished
	}
}

extension UISearchBar {
	func setupSearchBarTextfield(_ bgColor: UIColor, _ textColor: UIColor) {
		for subView in self.subviews  {
			for subsubView in subView.subviews  {
				if let textField = subsubView as? UITextField {
					textField.backgroundColor = bgColor
					textField.layer.cornerRadius = 6
					textField.textColor = textColor
				}
			}
		}
	}
}
