//
//  Forecast.swift
//  TestForecast
//
//  Created by Павел Привалов on 6/15/18.
//  Copyright © 2018 Павел Привалов. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class ForecastService: NSObject {

	func getForecast(lon: Double, lat: Double, completion: @escaping (JSON?) -> Void) {
		let APPID = "37b441395fd104c27a7b5409bd27559e"
		let userURL = "http://api.openweathermap.org/data/2.5/forecast"
		let parameters = ["lon": "\(lon)", "lat": "\(lat)", "appid": APPID, "cnt": "40", "units": "metric"]
		
		Alamofire.request(userURL, method: .get, parameters: parameters).validate().responseJSON {
			response in
			switch response.result {
			case .success:
				if let value = response.result.value {
					let json = JSON(value)
					completion(json)
				}
			case .failure:
				completion(nil)
				print("Error: Can't take weather forecast")
			}
		}
	}
	
}
