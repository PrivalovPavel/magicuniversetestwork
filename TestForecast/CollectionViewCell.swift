//
//  CollectionViewCell.swift
//  TestForecast
//
//  Created by Павел Привалов on 6/13/18.
//  Copyright © 2018 Павел Привалов. All rights reserved.
//

import UIKit

class CollectionViewCell: UICollectionViewCell {
	@IBOutlet weak var weatherImage: UIImageView!
	@IBOutlet weak var titleLabel: UILabel!
	@IBOutlet weak var detailLabel: UILabel!
	
	func setDataToCell(image: UIImage, title: String, detail: String) {
		weatherImage.image = image
		titleLabel.text = title
		detailLabel.text = detail
	}
}
