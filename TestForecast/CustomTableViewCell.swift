//
//  CustomTableViewCell.swift
//  TestForecast
//
//  Created by Павел Привалов on 6/14/18.
//  Copyright © 2018 Павел Привалов. All rights reserved.
//

import UIKit

class CustomTableViewCell: UITableViewCell {

	@IBOutlet weak var cityNameLabel: UILabel!
	@IBOutlet weak var rightImage: UIImageView!
	override func awakeFromNib() {
        super.awakeFromNib()
		rightImage.image = UIImage(named: "Right Detail")
	}

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }

}
