//
//  CityForecastViewController.swift
//  TestForecast
//
//  Created by Павел Привалов on 6/15/18.
//  Copyright © 2018 Павел Привалов. All rights reserved.
//

import UIKit
import CoreData
import SwiftyJSON

class Forecast {
	var weatherImage: UIImage
	var detailValues: [String]
	var temp: String
	var date: String
	
	init(weatherImage: UIImage, detailValues: [String], temp: String, date: String) {
		self.weatherImage = weatherImage
		self.detailValues = detailValues
		self.temp = temp
		self.date = date
	}
}

class CityForecastViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource {

	@IBOutlet weak var bigImage: UIImageView!
	@IBOutlet weak var tempLabel: UILabel!
	@IBOutlet weak var collectionViewCell: UICollectionView!
	@IBOutlet weak var detailCollectionView: UICollectionView!
	var city = City()
	var forecastJSON: JSON?
	let detailImages = ["Wind", "Chance", "Humidity"]
	var forecastDays = [Forecast]()
	var selectedIndex = 0
	
    override func viewDidLoad() {
        super.viewDidLoad()

		self.navigationItem.title = forecastJSON!["city"]["name"].string!
		self.navigationItem.rightBarButtonItem?.image = city.isSaved ? UIImage(named: "Delete") : UIImage(named: "add")
		
		collectionViewCell.setupCollectionViewCell(cellNumber: 5, leftMargin: 5, rightMargin: 5, isSquare: false)
		detailCollectionView.setupCollectionViewCell(cellNumber: 3, leftMargin: 25, rightMargin: 25, isSquare: false)
		getForecast()
		showWeather()
    }
	
	func showWeather() {
		bigImage.image = forecastDays[selectedIndex].weatherImage
		tempLabel.text = forecastDays[selectedIndex].temp
	}
	
	func getForecast() {
		var nowWeatherImage = UIImage()
		var detailValues: [String] = []
		var temp: String
		var date: String
		let dateFormater = DateFormatter()
		dateFormater.locale = Locale.init(identifier: "en_US")
		
		for i in 0...4
		{
			dateFormater.dateFormat = "yyyy-mm-dd hh:mm:ss"
			let dateObj = dateFormater.date(from: forecastJSON!["list"][i * 8]["dt_txt"].string!)
			print(forecastJSON!["list"][i * 8]["dt_txt"].string!)
			dateFormater.dateFormat = "EEEE"
			date = dateFormater.string(from: dateObj!)
			
			nowWeatherImage = self.getWeatherImage(type: forecastJSON!["list"][i * 8]["weather"][0]["main"].string)
			temp = String(format: "%.0f", (forecastJSON!["list"][i * 8]["main"]["temp"].double!)) + " ºC"
			detailValues.append(String(format: "%.0f", (forecastJSON!["list"][i * 8]["wind"]["speed"].double!)) + "m/s")
			detailValues.append(String(format: "%.0f", (forecastJSON!["list"][i * 8]["clouds"]["all"].double!)) + "%")
			detailValues.append(String(format: "%.0f", (forecastJSON!["list"][i * 8]["main"]["humidity"].double!)) + "%")
			
			forecastDays.append(Forecast(weatherImage: nowWeatherImage, detailValues: detailValues, temp: temp, date: date))
			detailValues.removeAll(keepingCapacity: false)
		}
	}

	func getWeatherImage(type: String?) -> UIImage
	{
		switch type {
		case "Thunderstorm":
			return UIImage(named: "Weather2_Big")!
		case "Drizzle", "Rain", "Snow":
			return UIImage(named: "Weather5_Big")!
		case "Atmosphere":
			return UIImage(named: "Weather1_Big")!
		case "Clear":
			return UIImage(named: "Weather3_Big")!
		case "Clouds":
			return UIImage(named: "Weather4_Big")!
		default:
			return UIImage(named: "Weather3_Big")!
		}
	}
	
	@IBAction func rightNavigationButton(_ sender: Any) {
		(sender as! UIBarButtonItem).image == UIImage(named: "add") ? saveCity() : deleteCity()
		navigationController?.popToRootViewController(animated: true)
	}
	
	func saveCity() {
		city.isSaved = true
		DatabaseController.saveContext()
	}
	
	func deleteCity() {
		city.isSaved = false
		DatabaseController.saveContext()
	}
	
	func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
		if collectionView == collectionViewCell {
			return 5
		} else {
			return 3
		}
	}
	
	func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
		if collectionView == collectionViewCell {
			let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "forecastCell", for: indexPath) as! CollectionViewCell
			cell.setDataToCell(image: forecastDays[indexPath.row].weatherImage, title: forecastDays[indexPath.row].temp, detail: forecastDays[indexPath.row].date)
			cell.contentView.backgroundColor = indexPath.row == selectedIndex ? UIColor.customOrange : UIColor.lightBackground
			return cell
		} else {
			let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "detailCell", for: indexPath) as! CollectionViewCell
			cell.setDataToCell(image: UIImage(named: detailImages[indexPath.row])!, title: forecastDays[selectedIndex].detailValues[indexPath.row], detail: detailImages[indexPath.row])
			return cell
		}
	}
	
	func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
		selectedIndex = indexPath.row
		showWeather()
		collectionViewCell.reloadData()
		detailCollectionView.reloadData()
	}
}
