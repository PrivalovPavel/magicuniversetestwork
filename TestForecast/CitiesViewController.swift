//
//  CitiesViewController.swift
//  TestForecast
//
//  Created by Павел Привалов on 6/13/18.
//  Copyright © 2018 Павел Привалов. All rights reserved.
//

import UIKit
import CoreData
import SwiftyJSON
import CoreLocation

class CitiesViewController: UIViewController, CLLocationManagerDelegate {

	@IBOutlet weak var collectionViewCell: UICollectionView!
	@IBOutlet weak var activityIndicator: UIActivityIndicatorView!
	
	let dispatchGroup = DispatchGroup()
	var cities = [City]()
	var nonFilteredCities = [City]()
	let forecast = ForecastService()
	var forecastJSON = [JSON]()
	var locationForecast = JSON()
	var locationCoordinate: [String: Double] = ["lon": 0, "lat": 0]
	var locationSegue: Bool = false
	let locationManager = CLLocationManager()
	var images = [UIImage]()
	var names = [String]()
	let segueButtonString = "Delete"
	var temps: [String] = []
	var selectedIndex = 0
	
    override func viewDidLoad() {
        super.viewDidLoad()
		
		locationManager.delegate = self
		locationManager.requestWhenInUseAuthorization()
		
		collectionViewCell.setupCollectionViewCell(cellNumber: 3, leftMargin: 5, rightMargin: 5, isSquare: true)
    }
	
	override func canPerformUnwindSegueAction(_ action: Selector, from fromViewController: UIViewController, withSender sender: Any) -> Bool {
		
		return false
	}

	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(false)
		
		locationManager.startUpdatingLocation()
		UIViewController.setFinishLoading(isFinished: false, activityIndicator: activityIndicator, hiddenView: collectionViewCell)
		loadCoreData()
	}
	
	func loadForecast() {
		self.images = []
		self.temps = []
		self.names = []
		self.forecastJSON = []
		for city in cities
		{
			getForecast(isForLocation: false, lon: city.lon, lat: city.lat)
		}
	}
	
	func getForecast(isForLocation: Bool, lon: Double, lat: Double) {
		forecast.getForecast(lon: lon, lat: lat) { (completion) in
			if completion != nil {
				print(completion!)
				if isForLocation
				{
					self.locationForecast = completion!
					self.dispatchGroup.leave()
				}
				else
				{
					self.images.append(self.getWeatherImage(type: completion!["list"][0]["weather"][0]["main"].string))
					self.temps.append(
						String(format: "%.0f", (completion!["list"][0]["main"]["temp_max"].double?.rounded())!) + "/" +
							String(format: "%.0f", (completion!["list"][0]["main"]["temp_min"].double?.rounded())!) + " ºC")
					self.names.append(completion!["city"]["name"].string!)
					self.forecastJSON.append(completion!)
					self.collectionViewCell.reloadData()
				}
			} else {
				let alert = UIAlertController(title: "Error", message: "We can't get forecast", preferredStyle: UIAlertControllerStyle.alert)
				alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
			}
		}
	}
	
	func loadCoreData() {
		let fetchRequest: NSFetchRequest<City> = City.fetchRequest()
		let queue = DispatchQueue.global(qos: .utility)
		let workItem = DispatchWorkItem {
			do
			{
				let cities = try DatabaseController.getContext().fetch(fetchRequest)
				self.nonFilteredCities = cities
				self.cities = self.nonFilteredCities.filter({ (city) -> Bool in
					return city.isSaved
				})
			}
			catch
			{
				print("error \(error)")
			}
		}
		queue.async(execute: workItem)
		workItem.notify(queue: DispatchQueue.main) {
			if self.nonFilteredCities.count == 0
			{
				UIViewController.setFinishLoading(isFinished: false, activityIndicator: self.activityIndicator, hiddenView: self.collectionViewCell)
				queue.async {
					self.loadJsonData(filename: "city.list")
					DispatchQueue.main.async {
						self.cities = self.nonFilteredCities.filter({ (city) -> Bool in
							return city.isSaved
						})
						self.loadForecast()
						self.collectionViewCell.reloadData()
						UIViewController.setFinishLoading(isFinished: true, activityIndicator: self.activityIndicator, hiddenView: self.collectionViewCell)
					}
				}
			}
			else
			{
				self.loadForecast()
				self.collectionViewCell.reloadData()
				UIViewController.setFinishLoading(isFinished: true, activityIndicator: self.activityIndicator, hiddenView: self.collectionViewCell)
			}
		}
	}
	
	func loadJsonData(filename: String) {
		if let path = Bundle.main.path(forResource: filename, ofType: "json")
		{
			do
			{
				let data = try Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe)
				let jsonResult = try JSONSerialization.jsonObject(with: data, options: .mutableLeaves)
				if let jsonResult = jsonResult as? [Dictionary<String, AnyObject>]
				{
					for result in jsonResult
					{
						let city: City = City(context: DatabaseController.getContext())
						
						city.id = result["id"] as! Int32
						city.name = result["name"] as? String
						if city.name == "Kiev" || city.name == "Hurzuf"
						{
							city.isSaved = true
						}
						city.country = result["country"] as? String
						city.lon = result["coord"]!["lon"] as! Double
						city.lat = result["coord"]!["lat"] as! Double
						
						nonFilteredCities.append(city)
					}
					DatabaseController.saveContext()
				}
			} catch {
				print("parse error: \(error)")
			}
		}
	}
	
	func getWeatherImage(type: String?) -> UIImage
	{
		switch type {
		case "Thunderstorm":
			return UIImage(named: "Weather2_Big")!
		case "Drizzle", "Rain", "Snow":
			return UIImage(named: "Weather5_Big")!
		case "Atmosphere":
			return UIImage(named: "Weather1_Big")!
		case "Clear":
			return UIImage(named: "Weather3_Big")!
		case "Clouds":
			return UIImage(named: "Weather4_Big")!
		default:
			return UIImage(named: "Weather3_Big")!
		}
	}
	
	override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
		if segue.identifier == "showSavedCity"
		{
			let newView = segue.destination as! CityForecastViewController
			
			if locationSegue {
				
				let filterID = locationForecast["city"]["id"].int32!
				let locationCity = nonFilteredCities.filter { (city) -> Bool in
					return city.id == filterID
				}.first!
				
				newView.forecastJSON = locationForecast
				newView.city = locationCity
				UIViewController.setFinishLoading(isFinished: true, activityIndicator: self.activityIndicator, hiddenView: self.collectionViewCell)
			} else {
				newView.forecastJSON = forecastJSON[selectedIndex]
				let cityID = forecastJSON[selectedIndex]["city"]["id"].int32
				newView.city = nonFilteredCities.filter({ (city) -> Bool in
					return city.id == cityID
				}).first!
			}
		}
	}
	
	func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
		guard let locationValue: CLLocationCoordinate2D = manager.location?.coordinate else { return }
		locationCoordinate["lon"] = locationValue.longitude
		locationCoordinate["lat"] = locationValue.latitude
	}
	
	@IBAction func locationButton(_ sender: Any) {
		dispatchGroup.enter()
		UIViewController.setFinishLoading(isFinished: false, activityIndicator: activityIndicator, hiddenView: collectionViewCell)
		locationSegue = true
		getForecast(isForLocation: true, lon: locationCoordinate["lon"]! , lat: locationCoordinate["lat"]!)
		dispatchGroup.notify(queue: DispatchQueue.main) {
			self.performSegue(withIdentifier: "showSavedCity", sender: nil)
		}
	}
	
	@IBAction func addButtonAction(_ sender: Any) {
		performSegue(withIdentifier: "searchTable", sender: nil)
	}
}

extension CitiesViewController: UICollectionViewDelegate, UICollectionViewDataSource
{
	func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
		return cities.count
	}
	
	func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
		let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! CollectionViewCell
		if indexPath.row < images.count
		{
			cell.setDataToCell(
				image: images[indexPath.row],
				title: names[indexPath.row],
				detail: temps[indexPath.row]
			)
		}
		return cell
	}
	
	func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
		self.selectedIndex = indexPath.row
		locationSegue = false
		performSegue(withIdentifier: "showSavedCity", sender: nil)
	}
}

extension UICollectionView {
	func setupCollectionViewCell(cellNumber: CGFloat, leftMargin: CGFloat, rightMargin: CGFloat, isSquare: Bool) {
		let cellSize = (UIScreen.main.bounds.width - (leftMargin + rightMargin)) / cellNumber - 4
		let layout = UICollectionViewFlowLayout()
		
		layout.sectionInset = UIEdgeInsetsMake(5, leftMargin, 5, rightMargin)
		layout.itemSize = CGSize(width: cellSize, height: isSquare ? cellSize : (self.frame.height - 10))
		layout.minimumLineSpacing = 4
		layout.minimumInteritemSpacing = 4
		
		self.collectionViewLayout = layout
	}
}

extension UIColor {
	
	static var darkBackground: UIColor {
		return UIColor(red: 25 / 255, green: 29 / 255, blue: 32 / 255, alpha: 1)
	}
	
	static var lightBackground: UIColor {
		return UIColor(red: 41 / 255, green: 46 / 255, blue: 49 / 255, alpha: 1)
	}
	
	static var customOrange: UIColor {
		return UIColor(red: 245 / 255, green: 130 / 255, blue: 35 / 255, alpha: 1)
	}
}
